package org.akbili.exo2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import org.akbili.model.Country;
import org.akbili.exo1.Question5;
import org.akbili.exo1.Question6;

public class Exo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Country> listCountries = new ArrayList<>();
		Country c1 = new Country("Allemgne", "B");
		Country c2 = new Country("Belgique", "A");
		
		listCountries.add(c1);
		listCountries.add(c2);

		
		//Stream<Country> stream1 = listCountries.stream();
		
		/******  QUESTION 1 ********/
		Comparator<Country> cName = (Country p1, Country p2) -> p1.getName().compareTo(p2.getName());
		
		listCountries.sort(cName);
		listCountries.forEach(item -> System.out.println(item));
		
		
		/******  QUESTION 2 ********/
		Stream<Country> stream1 = listCountries.stream();
		
		
		//CODE COMMENTE CAR INDIQUE UNE ERREUR (NON TROUVEE)
		/*
		
		public List<String> listCountryNamesFromGroup(String group) {
			return listPaysGroup.stream()
					.map(s -> s.getName())
					.filter(s -> s.equals(group))
					.collect(Collectors.toList());
					
		} */
		
		
		
		

	}

}
