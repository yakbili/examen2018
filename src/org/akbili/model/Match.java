package org.akbili.model;

import org.akbili.exo1.Question5;

public class Match {
	private Country countryA;
	private Country countryB;
	private int scoreA;
	private int scoreB;
	private String groupe;
	
	
	public Country getCountryA() {
		return countryA;
	}
	public void setCountryA(Country countryA) {
		this.countryA = countryA;
	}
	public Country getCountryB() {
		return countryB;
	}
	public void setCountryB(Country countryB) {
		this.countryB = countryB;
	}
	public int getScoreA() {
		return scoreA;
	}
	public void setScoreA(int scoreA) {
		this.scoreA = scoreA;
	}
	public int getScoreB() {
		return scoreB;
	}
	public void setScoreB(int scoreB) {
		this.scoreB = scoreB;
	}
	public String getGroupe() {
		return groupe;
	}
	public void setGroupe(String groupe) {
		this.groupe = groupe;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryA == null) ? 0 : countryA.hashCode());
		result = prime * result + ((countryB == null) ? 0 : countryB.hashCode());
		result = prime * result + ((groupe == null) ? 0 : groupe.hashCode());
		result = prime * result + scoreA;
		result = prime * result + scoreB;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Match other = (Match) obj;
		if (countryA == null) {
			if (other.countryA != null)
				return false;
		} else if (!countryA.equals(other.countryA))
			return false;
		if (countryB == null) {
			if (other.countryB != null)
				return false;
		} else if (!countryB.equals(other.countryB))
			return false;
		if (groupe == null) {
			if (other.groupe != null)
				return false;
		} else if (!groupe.equals(other.groupe))
			return false;
		if (scoreA != other.scoreA)
			return false;
		if (scoreB != other.scoreB)
			return false;
		return true;
	}
	public Match(Country countryA, Country countryB, int scoreA, int scoreB, String groupe) {
		super();
		this.countryA = countryA;
		this.countryB = countryB;
		this.scoreA = scoreA;
		this.scoreB = scoreB;
		this.groupe = groupe;
	}
	@Override
	public String toString() {
		return "Match [" + countryA + " - " + countryB + ", " + scoreA + " - " + scoreB+ "]";
	}
	
	
	
	
	
	
	
}
