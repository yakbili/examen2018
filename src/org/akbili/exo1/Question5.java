package org.akbili.exo1;

import java.util.function.Function;

import org.akbili.model.Country;

public class Question5 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/******  QUESTION 5A ********/
		String test = "A Russie";
		
		Function<String, String> lineToGroup = (String line) -> line.substring(0,1);
		
		System.out.println("test de lineToGroup" + lineToGroup.apply(test));
		
		/******  QUESTION 5B ********/
		Function<String, String> lineToName = (String line) -> line.split(" ")[1];
		System.out.println("test de lineToName" +lineToName.apply(test));

		/******  QUESTION 5C ********/
		Function<String, Country> lineToCountry = (String line) -> new Country(lineToName.apply(line), lineToGroup.apply(line));
		System.out.println(lineToCountry.apply(test));
		
		/******  QUESTION 6B ********/


		

	}

}
