package org.akbili.exo1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CountryAnalyze {
	
	public  List<String> readLinesFrom(String fileName) {
		Path path = Paths.get(fileName);
		try(Stream<String> lines = Files.lines(path)){
			return lines.collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	String fileName ="data.txt";
	
	public  List<String> readLinesData(){
		List<String> lines = readLinesFrom(fileName);
		return lines.stream()
				.collect(Collectors.toList());
}

}
