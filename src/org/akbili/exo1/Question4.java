package org.akbili.exo1;
//import  org.akbili.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Question4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<String> listPays = new ArrayList<>();
		
		listPays.add("Allemagne");
		listPays.add("France");
		listPays.add("Espagne");
		
		System.out.println("Liste avant tri : ");
		listPays.forEach(item -> System.out.println(item));
		
		Comparator<String> cPays = (String s1, String s2) -> s1.compareTo(s2);
		listPays.sort(cPays);
		
		System.out.println("Liste apres tri : ");

		listPays.forEach(item -> System.out.println(item));
		
		

	}

}
