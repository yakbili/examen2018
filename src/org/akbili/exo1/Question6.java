package org.akbili.exo1;

import java.util.List;
import java.util.stream.Collectors;

public class Question6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CountryAnalyze data = new CountryAnalyze();
		List<String> listPaysGroup = data.readLinesData().stream()
				.filter(line -> !line.isEmpty() && line.contains("#"))
				.collect(Collectors.toList());
		
		listPaysGroup.forEach(item -> System.out.println(item));



	}
}